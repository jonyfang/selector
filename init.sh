#!/bin/bash
brew install node
npm install
ps aux | grep "node ser" | grep -v "grep" | awk '{print $2}' | xargs kill -9
node server &
open "http://127.0.0.1:3000/"
